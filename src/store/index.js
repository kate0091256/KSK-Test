import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    ordersList: [],
    displayedOrders: [],
    isListView: true,
  },
  getters: {
    orders(state) {
      return state.ordersList;
    },
    displayed(state) {
      return state.displayedOrders;
    },
    isMainPageDisplayed(state) {
      return state.isListView;
    },
  },
  mutations: {
    initOrders(state, payload) {
      state.ordersList = payload;
      state.displayedOrders = payload;
    },
    setOrders(state, payload) {
      state.displayedOrders = payload;
    },
    setListView(state, payload) {
      state.isListView = payload;
    },
  },
  actions: {
  },
  modules: {
  },
});
